Tracking and improving own predictability using git, markdown and some ruby magic
=======

Predictability is not something which is easy and straightforward. When can
I expect this or that to be available on production? Is there any chance to
have this mandatory boring training to be completed till the end of month or
should I already ask for terms extension? The questions we often hear and ask.
I’ll share few personal wins and fuckups with planning/predictability topics
and show how I track it with git and markdown. Nothing special and some ruby
magic inside.

The main idea for week 32 (2022-08-08)
=======

Create some [nice youtube content](https://www.youtube.com/channel/UCOKlGnHjcPZmd_BiN3kuJ2Q)
Maybe about the tools, maybe about opensource. Who knows?

Predicatability level 0
-------

No predictability. Try different things and find out what is working and what is not.

Investments are locked. Alcohol and cigaretes are available with no restrictions.

Predicatability level 1 - my current one
-------

Low predictability. Work on establishing clear work-life balance and on making
work environment as much efficient as possible.

Points are obtained based on simple rules:
- one pomodoro gives one point
- starting work before 9am gives one point
- finishing work before 8pm gives one point
- morning pomodoro slot, appropriately started and finished before 10:30am gives
one point. Slot size does not matter.

Points are converted based on simple rules: 2 points into a pack of cigarettes;
5 points into a buttle of beer;
7 points into a IQOS VEEV vaping device.

Investments are possible up to €1k/month. Cigaretes are available only after
conversion or investment.

Predicatability level 2.
-------

Medium predictability. Keeping clear work-life balance and learning
as much as possible.

Points are obtained based on simple rules:
- one pomodoro gives one point
- starting work after 9am seizes one point
- finishing work after 8pm seizes one point
- Any pomodoro slot, appropriately started and finished gives one extra point.
Slot size should be not less than 2.

Points are converted based on simple rules: 3 points into a pack of cigarettes;
7 points into a buttle of beer;  11 points into a IQOS VEEV vaping device.

Investments are locked. Alcohol and cigaretes are available only after conversion.

Here is git+markdown workflow:
```
admin@SBVPN750 worklog % git commit --allow-empty -m "resume progress"
[master 2d00163] resume progress
admin@SBVPN750 worklog % git add . ;git commit -am "start focused slot"
[master f13ecdb] start focused slot
 1 file changed, 10 insertions(+)
admin@SBVPN750 worklog % git diff
diff --git a/2022-08-august/2022-08-13-notes.md b/2022-08-august/2022-08-13-notes.md
index 44bb138..62de4dd 100644
--- a/2022-08-august/2022-08-13-notes.md
+++ b/2022-08-august/2022-08-13-notes.md
@@ -1,4 +1,23 @@
+Pomodoro #1
+=======
+fun scriptiong
 
+Result
+-------
+- explain morning slots
+- explain worktime boundaries
+- explain pomodoros
+- add a note about levels and chat admin.
+
+Plan
+-------
+- explain morning slots
+- explain worktime boundaries
+- explain pomodoros
+
+Details
+-------
+одмін працює за півко: Следить за ссорами в чате,выдавать пермонентный бан
нарушителям,делать актив ввиде какого-либо контента
 
 Today I learned
 =======
admin@SBVPN750 worklog % git commit -am "slot result: 1"
[master 99c3e34] slot result: 1
 1 file changed, 19 insertions(+)
admin@SBVPN750 worklog % git commit --allow-empty -m "finish focused slot"
[master 9d560c1] finish focused slot
admin@SBVPN750 worklog % git log --format="%an (%ad): %s" --reverse -n 5
Petro Koriakin (Fri Aug 12 19:53:09 2022 +0300): pause progress
Petro Koriakin (Sat Aug 13 08:54:08 2022 +0300): resume progress
Petro Koriakin (Sat Aug 13 09:52:14 2022 +0300): start focused slot
Petro Koriakin (Sat Aug 13 10:18:04 2022 +0300): slot result: 1
Petro Koriakin (Sat Aug 13 10:18:10 2022 +0300): finish focused slot
admin@SBVPN750 worklog % 
```

Here is achievements script output
```
% ruby ../predictability-slides/source/lib/achievements.rb
|--------------------------------|
| Achievements: 10               |
|--------------------------------|
| Worktime boundaries: 4         |
| Morning slot: 1                |
| Pomodoros: 5                   |
|--------------------------------|
| Investments: 0 * 39.75 = 0 UAH |
|--------------------------------|
| Total score: 10                |
| Converted: 9                   |
| Availalbe free points: 1       |
|--------------------------------|

```

Predicatability level 3.
-------

Strong predictability. Try different things and find out what is working and
what is not.

Alcohol and cigaretes are available with no restrictions.

The main idea for week 30 (2022-07-25)
=======

Please, join
[Dedicated Telegram Channel](https://t.me/pomodoro_challenge)
for updates. I'm looking for advise in there and people are super friendly.
-------

Each Friday a poll starts in there: "How to spend investements?" The options
would be smth like `buy beers`, `host a party`, `pay into creditcard`,
`transfer to charity`.

Members of the chat can add their options. Voting is open and allows
multychoice. 

Option which collected the most votes by Sunday evening will be taken as
an action by @what_if_you_can 
**THE ONLY REQUIREMENT IS THAT EVERYONE VOTES. othervise money are raised next Friday**

Admin priveleges are granted to @Chomp3r 

Here is a list of items available for conversion:
-------

- **2 points can be converted into a pack of cigarettes**
- **5 points can be converted into a bottle of beer**
- **11 points can be converted into a IQOS VEEV vaping device**

The problem is that I need 12 packs of cigarettes each week. So then I
either work more efficiently or stop drinking or stop smoking.
Math is dead simple: `12 x 3 = 36`. In case I want few beers it turns
into `12 x 3 + 3 x 7 = 57`

The challenge will be to smoke only the cigarettes and to drink beers
which are obtained with points.
-------

I already tried this few times and dramatically failed )

Week 29
=======

| week | pomodoros | morning slots | worktime boundaries | total | converted |
|------|-----------|---------------|---------------------|-------|-----------|
| 21   | 15        | 2             | 2                   | 19    | 0         |
| 22   | 24        | 1             | 5                   | 30    | 0         |
| 23   | 11        | 3             | 3                   | 17    | 19        |
| 24   | 8         | 0             | 1                   | 9     | 0         |
| 25   | 15        | 1             | 2                   | 18    | 27        |
| 26   | 5         | 2             | 2                   | 9     | 0         |
| 27   | 10        | 3             | 5                   | 18    | 7         |
| 28   | 9         | 2             | 3                   | 14    | 35        |
| 29   | 12        | 2             | 2                   | 16    | 50        |

My pomodoro implementation QnA
=======

Q: what slot and break length do you use?
-------
Everything is classic: 25 minutes of work and then half-a-day of break.
The problem here is that zoom calls and incident mitigation can not be sliced to
timeslots. Therefore only mornings and late evenings are appropriate to do some
pomodoros.

Q: how long are you practicing pomodoro technique
-------
for three years already. At the beginning it was a nightmare: every instagram
notification was stoping the focused slot. Then, slowly, it appeared that I can
reply people in social networks and in messengers later (next week most probably).
The most dramatic insight was that I can hold answers for any slack message for
20-200 minutes. Almost everyone can wait.

Git is everywhere
=======

```bash
git commit --allow-empty -m "resume progress"

git add . ;git commit -am "start focused slot"
git commit -am "slot result: 3"
git commit --allow-empty -m "finish focused slot"
git log --format="%an (%ad): %s" --reverse -n 5

git commit --allow-empty -m "converted 9 points"

git add . ;git commit -am "some results and plans"; git commit --allow-empty -m "pause progress"
git commit --allow-empty -m "Kick CI"; git push origin


git log --format="%an (%ad): %s" --reverse -n 30 | grep "converted"
ruby ../predictability-slides/source/lib/total_scores.rb

```

Markdown should be readable
=======

![Week md templates](/talk-content/images/week_template.png "Week md templates")

!["Day md template"](/talk-content/images/day_template.png "Day md template")


Week 25 looks horrible
=======

```
ruby ../predictability-slides/source/lib/total_scores.rb
```

| week | pomodoros | morning slots | worktime boundaries | total | converted |
|------|-----------|---------------|---------------------|-------|-----------|
| 18   | 11        | 2             | 1                   | 14    | ??        |
| 19   | 9         | 2             | 5                   | 16    | ??        |
| 20   | 9         | 4             | 4                   | 17    | ??        |
| 21   | 15        | 2             | 2                   | 19    | ??        |
| 22   | 24        | 1             | 5                   | 30    | ??        |
| 23   | 11        | 3             | 3                   | 17    | ??        |
| 24   | 8         | 0             | 1                   | 9     | ??        |
| 25   | 15        | 1             | 2                   | 18    | ??        |
| 26   | 3         | 1             | 2                   | 6     | ??        |


Week 23
=======

![super cool bar chart for week 23](/talk-content/images/week_23_total_scores.png "Week 23 scores")


People who helped me with this everything in some way
=======

Dmytro Snisar https://www.facebook.com/dmitro.snisar
-------

Psychologist, Psychotherapist works as a head of research for NGO
“Mental Health Service - Association of mental health professionals”.
My major areas of research have focused on group interventions that might
be helpful after traumatic events or during prolonged stressful situations.
Besides the research activities, I am also trained as a psychotherapist
in Cognitive Behavior Therapy (CBT) and Eye movement desensitization
and reprocessing (EMDR). I have conducted training for combatants, provided
psychological first aid, crisis intervention and psychological support in the
war zone in the East of Ukraine since the beginning of the military conflict.


Olha Herus https://www.linkedin.com/in/olhaherus/
-------

International Certified Coach. Strong human resources professional with a
Master of Arts in Human Resources and Organisational Development from Lvi
 Business School of UCU (LvBS).

Russell Ramsay, PhD https://twitter.com/cbt4adhd
-------

Co-founder and co-director of PENN Adult ADHD Treatment and Research Program
(established in 1999). Hall of Fame inductee for CHADD (Children and Adults
with ADHD). Board Certified in cognitive-behavioral therapy (ABPP, AC&BT)

Olena Kalashnikova https://www.instagram.com/lenolen1/
-------

Certified Playback Practitioner of International Playback Theater
Network — IPTN. Affiliate trainer of CPT (Center of Playback Theater, NY, USA).
CEO, co-founder, conductor, actress in Playback Theater “Vachters”. Infinitely
in love with playback. She considers working with injured communities (victims
of military conflicts, victims of violence, etc.) to be her main focus in
playback. Also actively working with adolescents, LGBTQ+ community, addicts,
people subjected to gender discrimination, national minorities. Favorite topics:
Conducting, work with group dynamics, ritual, team building, work with trauma,
bodywork, metaphor, performance. Always open to new topics, proposals, developments.

My Biography
=======
I'm working in Tech since 2010. Ruby is one of my main tools since the very
beginning of my carrier.
When not working and especially at night I love craft beers and sometimes rum.
During the weekends I'm performing in the playback theatre
in Kyiv (https://dou.ua/lenta/articles/dou-hobby-playback/) and other cities
of Ukraine (https://www.facebook.com/MalePlaybackUA). I've been worked with
kottans.org community (https://youtube.com/playlist?list=PLEK9H5bICxvoiDKQ7epRpxUDmQotmvsgM)
in the past and now considering to resume that teaching activities.
My favorite sport is squash.
