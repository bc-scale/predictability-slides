# frozen_string_literal: true

require "spec_helper"

describe DataFetcher::Invesments do
  it "is a class" do
    expect(described_class).to be_a(Class)
  end

  describe "#current_week" do
    subject(:current_week) { described_class.new.current_week }

    it "returns integer" do
      expect(current_week).to be_an(Integer)
    end
  end

  describe "#raw_data" do
    subject(:raw_data) { described_class.new.raw_data }

    it "returns array" do
      expect(raw_data).to be_an(Array)
    end

    it "returns array of strings" do
      expect(raw_data.sample).to be_a(String)
    end
  end

  describe "#parsed_data" do
    subject(:parsed_data) { described_class.new.parsed_data }

    it "returns hash" do
      expect(parsed_data).to be_an(Hash)
    end

    it "returns a hash with data" do
      expect(parsed_data.slice(31)).to eq({ 31 => 80 })
    end
  end
end
