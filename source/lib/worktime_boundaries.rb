# frozen_string_literal: true

require "date"
require "pry"

# Report boudaries for last seven weeks
# week 15 - 0
# week 16 - 0
# week 17 - 0
# week 18 - 0
# week 19 - 0
# week 20 - 0
# week 21 - 2
# week 22 - 6

# week 21 - 2
# 0 Petro Koriakin (Mon May 23 09:45:15 2022 +0300): resume progress
# 0 Petro Koriakin (Mon May 23 21:40:49 2022 +0300): pause progress
# 0 Petro Koriakin (Tue May 24 10:11:19 2022 +0300): resume progress
# 0 Petro Koriakin (Tue May 24 20:12:18 2022 +0300): pause progress
# 1 Petro Koriakin (Fri May 27 07:24:58 2022 +0300): resume progress
# 0 Petro Koriakin (Sun May 29 14:33:43 2022 +0300): resume progress
# 1 Petro Koriakin (Sun May 29 16:33:43 2022 +0300): pause progress

# week 22 - 5 (15 invested)
# 1 Petro Koriakin (Mon May 30 07:57:18 2022 +0300): resume progress
# 1 Petro Koriakin (Mon May 30 19:56:55 2022 +0300): pause progress
# 1 Petro Koriakin (Tue May 31 08:53:48 2022 +0300): resume progress
# 0 Petro Koriakin (Tue May 31 20:13:54 2022 +0300): pause progress
# 0 Petro Koriakin (Wed Jun 1 10:09:39 2022 +0300): resume progress
# 0 Petro Koriakin (Wed Jun 1 22:30:15 2022 +0300): pause progress
# 0 Petro Koriakin (Fri Jun 3 11:38:53 2022 +0300): resume progress
# 1 Petro Koriakin (Fri Jun 3 19:53:49 2022 +0300): pause progress
# 1 Petro Koriakin (Sat Jun 4 09:00:58 2022 +0300): resume progress

def timeframe
  (Date.today.cweek - 8)
end

def last_seven_weeks?(finish_timestamp)
  finish_timestamp.cweek >= timeframe
end

def morning_time?(timestamp)
  return true if timestamp.hour < 9
  return true if timestamp.hour == 9 && timestamp.minute <= 1

  false
end

def evening_time?(timestamp)
  return true if timestamp.hour < 20
  return true if timestamp.hour == 20 && timestamp.minute <= 1

  false
end

def parse_log_time(log_entrie)
  raw_date = log_entrie.split("(").last.split(")").first
  DateTime.parse(raw_date)
end

raw_data_starts = `git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "resume progress"`
raw_data_finishes = `git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "pause progress"`

morning_results = raw_data_starts.split("\n").map { |start_log| parse_log_time(start_log) }
  .select { |timestamp| last_seven_weeks?(timestamp) && morning_time?(timestamp) }
  .group_by(&:cweek)
evening_results = raw_data_finishes.split("\n").map { |finish_log| parse_log_time(finish_log) }
  .select { |timestamp| last_seven_weeks?(timestamp) && evening_time?(timestamp) }
  .group_by(&:cweek)
(timeframe..Date.today.cweek).each do |week|
  puts "week #{week} - #{morning_results.fetch(week, []).size + evening_results.fetch(week, []).size}"
end
