# frozen_string_literal: true

require "gruff"
require "pry"

g = Gruff::Bar.new
g.title = "Your Recent Seven Weeks Progress"
g.theme = Gruff::Themes::RAILS_KEYNOTE
g.legend_font_size = 16
g.marker_font_size = 16
g.labels = {
  0 => "week17: 11",
  1 => "week18: 14",
  2 => "week19: 16",
  3 => "week20: 17",
  4 => "week21: 19",
  5 => "week22: 30",
  6 => "week23: 19",
}
g.data(:Pomodorros, [7, 11, 9, 9, 15, 24, 12], "#990000")
g.data(:Morning_Slots, [2, 2, 2, 4, 2, 1, 3], "#009900")
g.data(:Worktime_Boundaries, [2, 1, 5, 4, 2, 5, 4], "#990099")

g.minimum_value = 0

g.write("charts/week_23_total_scores.png")
