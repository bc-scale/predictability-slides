# frozen_string_literal: true

require "date"
require "pry"
require "pp"

# Report pomodoros for last seven weeks
# week 13 - 1
# week 14 - 4
# week 15 - 8
# week 16 - 5
# week 17 - 7
# week 18 - 11
# week 19 - 9
# week 20 - 11
# week 21 - 15

def timeframe
  (Date.today.cweek - 8)
end

def last_seven_weeks?(finish_timestamp)
  finish_timestamp.cweek >= timeframe
end

# takes string like
#   "2022-01-january/2022-01-11-notes.md",
#   "2022-01-january/2022-01-23-week-04.md",
# returns
#   <Date: 2022-01-11 ((2459515j,0s,0n),+0s,2299161j)>,
#   <Date: 2022-01-23 ((2459514j,0s,0n),+0s,2299161j)>,
def extract_date(markdown_filename)
  Date.parse(markdown_filename.split("/").last.split("-notes").first.split("-week").first)
end

# TODO: handle the case when Sunday was also a working day with some pomodoros
def count_pomodorros_for(week, data)
  data.select { |date, _values| date.cweek == week }.values
    .map { |item| item.map { |i| i.last.split("#").last.split("-").last.to_i }.max }
    .sum
end

raw_data = `grep -r "Pomodoro #" ../../worklog`
parsed_data = raw_data.split("\n").map { |i| i.split(":") }
  .delete_if { |raw_item| raw_item.first.match?(".rb") }
  .group_by { |raw_item| extract_date(raw_item.first) }
  .select { |key, _value| last_seven_weeks?(key) }

(timeframe..Date.today.cweek).each { |week| puts "week #{week} - #{count_pomodorros_for(week, parsed_data)}" }
