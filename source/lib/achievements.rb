# frozen_string_literal: true

require "date"
require "terminal-table"
require_relative "data_fetcher/morning_slots"
require_relative "data_fetcher/converted_points"
require_relative "data_fetcher/pomodoros"
require_relative "data_fetcher/investments"
require_relative "data_fetcher/worktime_boundaries"
require_relative "analytics/weekly"

# % ruby ../predictability-slides/source/lib/achievements.rb
# |--------------------------------|
# | Achievements: 9                |
# |--------------------------------|
# | Worktime boundaries: 3         |
# | Morning slot: 1                |
# | Pomodoros: 5                   |
# |--------------------------------|
# | Investments: 0 * 41.79 = 0 UAH |
# |--------------------------------|
# | Total score: 9                 |
# | Converted: 9                   |
# | Investment: 0 = 9 - 9          |
# |--------------------------------|

data = Analytics::Weekly.new

rows = []
rows << :separator
rows << ["Achievements: #{data.total}"]
rows << :separator
rows << ["Worktime boundaries: #{data.worktime_boundaries}"]
rows << ["Morning slot: #{data.morning_slots}"]
rows << ["Pomodoros: #{data.pomodoros}"]
rows << :separator
rows << ["Investments: #{data.current_result}"]
rows << :separator
rows << ["Total score: #{data.total}"]
rows << ["Converted: #{data.converted_points}"]
rows << ["Availalbe free points: #{data.total - data.converted_points + (data.invesments / 10)}"]
rows << :separator
table = Terminal::Table.new rows: rows
table.style = { border: :markdown }
puts table
