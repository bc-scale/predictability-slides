# frozen_string_literal: true

module DataFetcher
  class Pomodoros
    def initialize
      data = `grep -r "Pomodoro #" ../../worklog`
      @raw_pomodoro_data = data.split("\n").map { |i| i.split(":") }
        .delete_if { |raw_item| raw_item.first.match?(".rb") }
        .group_by { |raw_item| extract_date(raw_item.first) }
    end

    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      (0..Date.today.cweek).each_with_object(Hash.new(0)) do |week, hash|
        hash[week] = raw_pomodoro_data.select { |date, _values| date.cweek == week }.values
          .map { |item| item.map { |i| i.last.split("#").last.split("-").last.to_i }.max }
          .sum
      end
    end

    private

    attr_reader :raw_pomodoro_data

    def parse_converted_value(log_entry)
      log_entry.split(" converted ").last.to_i
    end

    def parse_log_time(log_entry)
      DateTime.parse(log_entry.match(/(?<=\().+?(?=\))/)[0])
    end

    # takes string like
    #   "2022-01-january/2022-01-11-notes.md",
    #   "2022-01-january/2022-01-23-week-04.md",
    # returns
    #   <Date: 2022-01-11 ((2459515j,0s,0n),+0s,2299161j)>,
    #   <Date: 2022-01-23 ((2459514j,0s,0n),+0s,2299161j)>,
    def extract_date(markdown_filename)
      Date.parse(markdown_filename.split("/").last.split("-notes").first.split("-week").first)
    end
  end
end
