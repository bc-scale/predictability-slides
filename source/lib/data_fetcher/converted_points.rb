# frozen_string_literal: true

module DataFetcher
  # Here is a data which exists
  #   % git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "converted"
  # Petro Koriakin (Tue Jun 7 14:47:12 2022 +0300): converted 3 points from this week
  # Petro Koriakin (Tue Jun 7 21:42:27 2022 +0300): converted 3 points from this week
  # Petro Koriakin (Thu Jun 9 09:37:01 2022 +0300): converted 4 points from this week
  # Petro Koriakin (Thu Jun 9 21:15:48 2022 +0300): converted 3 points from this week
  # Petro Koriakin (Thu Jun 9 22:40:58 2022 +0300): converted 6 points from this week
  # Petro Koriakin (Mon Jun 20 14:54:08 2022 +0300): converted 9 points
  # Petro Koriakin (Tue Jun 21 19:33:16 2022 +0300): converted 6 points
  # Petro Koriakin (Wed Jun 22 22:50:25 2022 +0300): converted 6 points
  # Petro Koriakin (Sun Jun 26 18:22:03 2022 +0300): converted 6 points
  # Petro Koriakin (Tue Jul 5 20:12:47 2022 +0300): converted 7 points
  # Petro Koriakin (Tue Jul 12 20:06:48 2022 +0300): converted 14 points
  # Petro Koriakin (Wed Jul 13 09:39:23 2022 +0300): converted 14 points
  # Petro Koriakin (Fri Jul 15 22:55:11 2022 +0300): converted 7 points
  #
  # And here is #parsed_data output
  #  { 25=>27, 26=>0, 27=>7, 28=>35 }

  class ConvertedPoints
    def raw_data
      `#{command}`.split("\n")
    end

    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      raw_data.each_with_object(Hash.new(0)) do |log_entry, result|
        result[parse_log_time(log_entry).cweek] += parse_converted_value(log_entry)
      end
    end

    private

    def parse_converted_value(log_entry)
      log_entry.split(" converted ").last.to_i
    end

    def parse_log_time(log_entry)
      DateTime.parse(log_entry.match(/(?<=\().+?(?=\))/)[0])
    end

    def command
      <<~SLOT_CMD
        git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse --grep "converted"
      SLOT_CMD
    end
  end
end
