# frozen_string_literal: true

module DataFetcher
  class WorktimeBoundaries
    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      (0..Date.today.cweek).each_with_object(Hash.new(0)) do |week, hash|
        hash[week] += morning_boundaries.fetch(week, []).size
        hash[week] += evening_boundaries.fetch(week, []).size
      end
    end

    private

    def morning_boundaries
      @morning_boundaries ||= raw_data_starts.map { |start_log| parse_log_time(start_log) }
        .select { |timestamp| morning_border_time?(timestamp) }
        .group_by(&:cweek)
    end

    def evening_boundaries
      @evening_boundaries ||= raw_data_finishes.map { |finish_log| parse_log_time(finish_log) }
        .select { |timestamp| evening_border_time?(timestamp) }
        .group_by(&:cweek)
    end

    def raw_data_starts
      `#{starts_cmd}`.split("\n")
    end

    def raw_data_finishes
      `#{finishes_cmd}`.split("\n")
    end

    def morning_border_time?(timestamp)
      return true if timestamp.hour < 9
      return true if timestamp.hour == 9 && timestamp.minute <= 1

      false
    end

    def evening_border_time?(timestamp)
      return true if timestamp.hour < 20
      return true if timestamp.hour == 20 && timestamp.minute <= 1

      false
    end

    def parse_log_time(log_entry)
      DateTime.parse(log_entry.match(/(?<=\().+?(?=\))/)[0])
    end

    def starts_cmd
      <<~STARTS_CMD
        git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse --grep "resume progress"
      STARTS_CMD
    end

    def finishes_cmd
      <<~FINISHES_CMD
        git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse --grep "pause progress"
      FINISHES_CMD
    end
  end
end
