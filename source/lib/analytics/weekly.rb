# frozen_string_literal: true

module Analytics
  class Weekly
    def total
      morning_slots + worktime_boundaries + pomodoros
    end

    def morning_slots
      morning_slots_fetcher.current_week
    end

    def worktime_boundaries
      worktime_boundaries_fetcher.current_week
    end

    def pomodoros
      pomodoros_fetcher.current_week
    end

    def converted_points
      converted_points_fetcher.current_week
    end

    def invesments
      investments_fetcher.current_week
    end

    def current_result
      "#{invesments} * #{uah_to_eur_course} = #{(invesments * uah_to_eur_course).to_i} UAH"
    end

    private

    def uah_to_eur_course
      41.79
    end

    def morning_slots_fetcher
      DataFetcher::MorningSlots.new
    end

    def worktime_boundaries_fetcher
      DataFetcher::WorktimeBoundaries.new
    end

    def pomodoros_fetcher
      DataFetcher::Pomodoros.new
    end

    def converted_points_fetcher
      DataFetcher::ConvertedPoints.new
    end

    def investments_fetcher
      DataFetcher::Invesments.new
    end
  end
end
