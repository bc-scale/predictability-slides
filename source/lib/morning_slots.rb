# frozen_string_literal: true

require "date"
require "pry"

# Report morning slots for last seven weeks
# week 13 - 0
# week 14 - 1
# week 15 - 4
# week 16 - 3
# week 17 - 2
# week 18 - 2
# week 19 - 2
# week 20 - 4
# week 21 - 2

def timeframe
  (Date.today.cweek - 8)
end

def morning_time?(finish_timestamp)
  return true if finish_timestamp.hour < 10
  return true if finish_timestamp.hour == 10 && finish_timestamp.minute <= 30

  false
end

def last_seven_weeks?(finish_timestamp)
  finish_timestamp.cweek >= timeframe
end

def parse_finish_time(tuple)
  raw_date = tuple.last.split("(").last.split(")").first
  DateTime.parse(raw_date)
end

def valid?(tuple)
  return false unless tuple.size == 3
  return false unless tuple.first.match?("start focused")
  return false unless tuple[1].match?("slot result")
  return false if tuple[1].match?("slot result: 0")
  return false unless tuple.last.match?("finish focused")

  true
end

raw_data = `cd ../../worklog; git log --format="%an (%ad): %s" --reverse | grep "start focused slot" -A 2`
items = raw_data.split("--\n")
tuples = items.map { |i| i.split("\n") }
week_results = tuples.select { |tuple| valid?(tuple) }
  .map { |tuple| parse_finish_time(tuple) }
  .select { |finish_time| last_seven_weeks?(finish_time) && morning_time?(finish_time) }
  .group_by(&:cweek)
(timeframe..Date.today.cweek).each { |week| puts "week #{week} - #{week_results.fetch(week, []).size}" }
