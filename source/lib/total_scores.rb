# frozen_string_literal: true

require "date"
require "terminal-table"
require_relative "data_fetcher/morning_slots"
require_relative "data_fetcher/converted_points"

# | week | pomodoros | morning slots | worktime boundaries | total | converted |
# |------|-----------|---------------|---------------------|-------|-----------|
# | 22   | 24        | 1             | 5                   | 30    | 0         |
# | 23   | 11        | 3             | 3                   | 17    | 19        |
# | 24   | 8         | 0             | 1                   | 9     | 0         |
# | 25   | 15        | 1             | 2                   | 18    | 27        |
# | 26   | 5         | 2             | 2                   | 9     | 0         |
# | 27   | 10        | 3             | 5                   | 18    | 7         |
# | 28   | 9         | 2             | 3                   | 14    | 35        |
# | 29   | 11        | 2             | 2                   | 15    | 50        |
# | 30   | 13        | 1             | 5                   | 19    | 56        |

def timeframe
  (Date.today.cweek - 8)
end

def morning_time?(finish_timestamp)
  return true if finish_timestamp.hour < 10
  return true if finish_timestamp.hour == 10 && finish_timestamp.minute <= 30

  false
end

def last_seven_weeks?(finish_timestamp)
  finish_timestamp.cweek >= timeframe
end

def parse_finish_time(tuple)
  raw_date = tuple.last.split("(").last.split(")").first
  DateTime.parse(raw_date)
end

def valid?(tuple)
  return false unless tuple.size == 3
  return false unless tuple.first.match?("start focused")
  return false unless tuple[1].match?("slot result")
  return false if tuple[1].match?("slot result: 0")
  return false unless tuple.last.match?("finish focused")

  true
end

items = DataFetcher::MorningSlots.new.raw_data
converted_points = DataFetcher::ConvertedPoints.new.parsed_data
tuples = items.map { |i| i.split("\n") }
morning_slot_results = tuples.select { |tuple| valid?(tuple) }
  .map { |tuple| parse_finish_time(tuple) }
  .select { |finish_time| last_seven_weeks?(finish_time) && morning_time?(finish_time) }
  .group_by(&:cweek)

def morning_border_time?(timestamp)
  return true if timestamp.hour < 9
  return true if timestamp.hour == 9 && timestamp.minute <= 1

  false
end

def evening_border_time?(timestamp)
  return true if timestamp.hour < 20
  return true if timestamp.hour == 20 && timestamp.minute <= 1

  false
end

def parse_log_time(log_entrie)
  raw_date = log_entrie.split("(").last.split(")").first
  DateTime.parse(raw_date)
end

starts_cmd = <<~STARTS_CMD
  git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "resume progress"
STARTS_CMD
finishes_cmd = <<~FINISHES_CMD
  git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "pause progress"
FINISHES_CMD
raw_data_starts = `#{starts_cmd}`
raw_data_finishes = `#{finishes_cmd}`

morning_boundaries = raw_data_starts.split("\n").map { |start_log| parse_log_time(start_log) }
  .select { |timestamp| last_seven_weeks?(timestamp) && morning_border_time?(timestamp) }
  .group_by(&:cweek)
evening_boundaries = raw_data_finishes.split("\n").map { |finish_log| parse_log_time(finish_log) }
  .select { |timestamp| last_seven_weeks?(timestamp) && evening_border_time?(timestamp) }
  .group_by(&:cweek)

# takes string like
#   "2022-01-january/2022-01-11-notes.md",
#   "2022-01-january/2022-01-23-week-04.md",
# returns
#   <Date: 2022-01-11 ((2459515j,0s,0n),+0s,2299161j)>,
#   <Date: 2022-01-23 ((2459514j,0s,0n),+0s,2299161j)>,
def extract_date(markdown_filename)
  Date.parse(markdown_filename.split("/").last.split("-notes").first.split("-week").first)
end

def count_pomodorros_for(week, data)
  data.select { |date, _values| date.cweek == week }.values
    .map { |item| item.map { |i| i.last.split("#").last.split("-").last.to_i }.max }
    .sum
end

raw_pomodoro_data = `grep -r "Pomodoro #" ../../worklog`
parsed_pomodoro_data = raw_pomodoro_data.split("\n").map { |i| i.split(":") }
  .delete_if { |raw_item| raw_item.first.match?(".rb") }
  .group_by { |raw_item| extract_date(raw_item.first) }
  .select { |key, _value| last_seven_weeks?(key) }
# raw_pomodoro_data = `grep -r "Pomodoro #" ../../worklog`
# parsed_pomodoro_data = raw_pomodoro_data.split("\n").map { |i| i.split(":") }
#   .delete_if { |raw_item| raw_item.first.match?(".rb") }
#   .group_by { |raw_item| extract_date(raw_item.first) }
#   .select { |key, _value| last_seven_weeks?(key) }

rows = []
rows << ["week", "pomodoros", "morning slots", "worktime boundaries", "total", "converted"]
rows << :separator
(timeframe..Date.today.cweek).each do |week|
  row = [week]
  total = morning_slot_results.fetch(week, []).size
  total += morning_boundaries.fetch(week, []).size
  total += evening_boundaries.fetch(week, []).size
  total += count_pomodorros_for(week, parsed_pomodoro_data)
  row << count_pomodorros_for(week, parsed_pomodoro_data)
  row << morning_slot_results.fetch(week, []).size
  row << (morning_boundaries.fetch(week, []).size + evening_boundaries.fetch(week, []).size)
  row << total
  row << converted_points.fetch(week, 0)
  rows << row
end

table = Terminal::Table.new rows: rows
table.style = { border: :markdown }
puts table
# `say -v Thomas "Petro, you are doing hreat things. Please, continue"`
