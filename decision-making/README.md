# Executive Summary

I'll join a cool company. Both offers are pretty sweet and have equal score atm: 7.9! Most probably I'll have to follow my heart...

# Methodology

We aligned our vote strength with the team.
The votes are given based on 0-10 scale by each teammate.

*Please, note: the scores and draft weights are both not final. I'm still in the middle of the interview processes with both companies.*


# Meet the team

## Overoptimistic Bird [vote rank is 0.3]

Wants fullfilment and to live meaninfull life. Fears stop evolving and learning

## The Crazy Frog  [vote rank is 0.1]

Wants love and belonging. Fears to be lonely in this cold world.

## Just a King of Night [vote rank is 0.2]

Wants discipline and fears to loose the power. Bigger salary is a prio.

## Successful Thriving Engineer  [vote rank is 0.3]

Wants well defined processes and fears to be critisized by others

## Baaabushga [vote rank is 0.1]

Wants safety and fears to get sick. Psychological safety is a prio


# Votes and reasoning

##  `SoftServe` proposal

| Name                | Score | Reasoning for the vote score |
| ------------------- | ----- |----------------------------- |
| Baaabushga          | 10    | all good. health insurance. Psychological safety is a key in there |
| Engineer            | 10    | Documentation first! Iteration! Feedbaaack! |
| King of Night       | 03    | Clear carrier guidelines and a lot of obstackles for salary increases. |
| Crazy Frog          | 09    | Old friends. Transpareancy. Collaboration! |
| Overoptimistic Bird | 08    | Super inspiring opportunity. Open core. Open source Impact of my works is huge. Growth and learning are heavily regulated|



##  `one cool European company` proposal

| Name                | Score | Reasoning for the vote score |
| ------------------- | ----- |----------------------------- |
| Baaabushga          | 07    | risks. paymants... no clarity. But everything is transparently documented |
| Engineer            | 07    | Agrhhh... Processes and documentation are not so great. And I can make a ton of impact by fixing that |
| King of Night       | 07    | A ton of flexibility with income stream: if I work more, I earn more. |
| Crazy Frog          | 07    | Not so dynamic environment. All people are friendly still |
| Overoptimistic Bird | 10    | The opportunity is sweet enough. Needs some more inspiration. Business impact is super welcomed and definitely possible |


##  Decision weight calculation

```ruby
# SoftServe => 7.9
(0.1 * 10) + (0.3 * 10) +  (0.2 * 3) +  (0.1 * 9) +  (0.3 * 8)

# one cool European company => 7.9
(0.1 * 7) + (0.3 * 7) +  (0.2 * 7) +  (0.1 * 7) +  (0.3 * 10)

```
