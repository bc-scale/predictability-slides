# What is your name?

Petro Korkiakin

# When is your birthday?

October 22, 1986

# What is your job position?

Backend Software Engineer

# Where are you from?

Moscow region, Russian Federation

# Where are you based?

Kyiv, Ukraine

# What are some of your hobbies?

Playback theater

# What does your typical remote working day look like?

- 8am - waking up, have a breakfast, walk through social media
- ~10am - starting work.
- 2pm or so - lunch and loong walk. Maybe some out-of-work appointments.
- 7pm - wrap up my day, switching into nonworking mode.
- A note: I tend to practice late stays and over-the-weekend work. It is quite unhealthy...

# What do you like to do when you finish your working day?

Playback theater rehearsals, social media, cooking, watch some Netflix

# What are some of your favorite movies?

Dune, LOTR trilogy, The Matrix (all of them)

# Are you a morning person, or a night owl?

hard to tell exactly. Rather night owl who adopted to start earlier

# How would you describe your personality?

When thinking about my personality, I tend to reffer Jeffry Young's schema therapy concepts. It allows me to segregate and impersonate different sides of me:
- Overoptimistic Bird: Wants fullfilment and to live meaninfull life. Fears stop evolving and learning
- The Crazy Frog: Wants love and belonging. Fears to be lonely in this cold world.
- Just a King of Night: Wants discipline and fears to loose the power. Bigger salary is a prio.
- Successful Thriving Engineer: Wants well defined processes and fears to be critisized by others
- Baaabushga: Wants safety and fears to get sick. Psychological safety is a prio

# What is your favorite season?

All of them are great, especially Spring

# What is your favorite place to travel to?

My parents' house in Myrhorod, Ukraine atm. All other options are temporarily unavaialble

# Is there anyone that inspires you and why? What qualities do you admire in them?

Hard to answer. I'll return to this later. Usually I get inspired by people around me

# What is your favorite book?

The one I'm reading right now

# What is your favorite style of music?

Punk rock. Blue-grass country.

# What is your proudest accomplishment?

Something in the future. Nothing to mention for now.

# Cats or dogs?

Cats )

# What is your favorite sport?

Squash

# When working on a project, which skill of yours can usually propel it the most?

Creativity. Ability to find smart workarounds.

# What trait of yours do you dislike?

Overthinking. Freeze-the-world syndrome. Imposter syndrome.

# What brings you the most satisfaction when working within a team?

Great revenue gains and new sales prospects.

# Is there anything that can negatively affect your mood at work?

Lack of transparency. Lack of direct timely actionable feedback.

# Do you have any personal limitations that are preventing you from doing work at best (the idea is to see if everyone is feeling okay or they have some issues or problems that are preventing them from working their best. For example it could be depression, chronic pain, etc)?

Overthinking. I tend to procrastinate actions and think about details shallow instead.
Overoptimistic perceptions about the future. This leads to inadequate estimates often. 

Please, stop me when I start writing looong emails or slack threads by asking simple yes/no questions

# My Biography

I'm working in Tech since 2010. Ruby is one of my main tools since the very
beginning of my carrier. When not working and especially at night I love craft beers and sometimes rum. During the weekends I'm [performing in the playback theatre](https://dou.ua/lenta/articles/dou-hobby-playback/) in Kyiv and [other cities of Ukraine](https://www.facebook.com/MalePlaybackUA). I've been worked with [kottans.org community](https://youtube.com/playlist?list=PLEK9H5bICxvoiDKQ7epRpxUDmQotmvsgM) in the past and now considering to resume that teaching activities. My favorite sport is squash. [Feel free to schedule some great talk with me](https://calendly.com/petrokoriakin/talk-to-petro?month=2022-12)